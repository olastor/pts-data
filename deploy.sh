#!/bin/bash

HOST_DIR="/testing"
LATEST_RELEASE=$(find $HOST_DIR -name "*.tar.gz" | sort | tail -n 1)

if [[ -f $LATEST_RELEASE ]]; then
  echo "Found archive $LATEST_RELEASE"
  tar xfvz $LATEST_RELEASE -C $HOST_DIR
  rm $LATEST_RELEASE
fi
