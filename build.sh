#!/bin/bash

set -e

python3 src/datasources/anbts/main.py
python3 src/datasources/tporg/main.py
python3 src/datasources/scbil/main.py
python3 src/build_api.py