from os import path
from glob import glob
from collections import defaultdict
from time import time
import json
import re
import subprocess

with open(path.join(path.dirname(path.realpath(__file__)), '../../../metadata/books.json')) as f:
  BOOK_NAMES = json.loads(f.read())

INT_TO_ROMAN = ['', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X']
ROMAN_TO_INT = { r: i for i, r in enumerate(INT_TO_ROMAN) }

log_started = None
def log(msg: str, prefix = '->'):
  global log_started

  if msg.lower().strip() == 'done' and log_started:
    print('%s %s (%is)' % (prefix, msg, time() - log_started))
  else:
    log_started = time()
    print('%s %s' % (prefix, msg))

class ExtractReferences:
  def __init__(self, bilara_dir, sc_data_dir, use_cache = False):
    self.BILARA_DIR = bilara_dir
    self.SC_DATA_DIR = sc_data_dir
    self.REFERENCE_DIR = path.join(self.BILARA_DIR, 'reference/pli/ms/')
    self.REFERENCE_FILES = glob(path.join(self.REFERENCE_DIR, 'vinaya','**/*.json'), recursive=True) + glob(path.join(self.REFERENCE_DIR, 'sutta','**/*.json'), recursive=True) + glob(path.join(self.REFERENCE_DIR, 'abhidhamma','**/*.json'), recursive=True)

    self.BILARA_JSON_TRANSLATION_FILES = glob(path.join(self.BILARA_DIR, 'translation','**/*.json'), recursive=True)
    self.HTML_FILES = glob(path.join(self.SC_DATA_DIR, 'html_text', '**/*.html'), recursive=True)

    with open(path.join(path.dirname(__file__), '../../../metadata/books.json')) as f:
      self.BOOK_ABBREVS = json.loads(f.read())

    with open(path.join(self.SC_DATA_DIR, 'misc/uid_expansion.json')) as f:
      self.UID_EXPANSION = { item['uid']: item for item in json.loads(f.read()) }

    with open(path.join(self.SC_DATA_DIR, 'additional-info/author_edition.json')) as f:
      self.SC_AUTHORS = json.loads(f.read())

    with open(path.join(self.BILARA_DIR, '_author.json')) as f:
      self.BILARA_AUTHORS = json.loads(f.read())

    self.SC_AUTHOR_LONG_TO_ID = { x['long_name']: x['uid'] for x in self.SC_AUTHORS }
    self.SC_AUTHOR_SHORT_TO_ID = { x['short_name']: x['uid'] for x in self.SC_AUTHORS }
    self.SC_AUTHOR_ID_TO_LONG = { x['uid']: x['long_name'] for x in self.SC_AUTHORS }

    log('creating text id to name mapping')
    text_id_to_name_file = path.join(path.dirname(path.realpath(__file__)), '../data/cache/text_id_to_name.json')
    if use_cache and path.exists(text_id_to_name_file):
      with open(text_id_to_name_file) as f:
        self.TEXT_ID_TO_NAME = json.loads(f.read())
    else:
      self.TEXT_ID_TO_NAME = self.get_text_name_mapping()
      with open(text_id_to_name_file, 'w') as f:
        f.write(json.dumps(self.TEXT_ID_TO_NAME))
    log('done')

    log('creating html file to author mapping')
    sc_html_file_to_author_file = path.join(path.dirname(path.realpath(__file__)), '../data/cache/sc_html_file_to_author.json')
    if use_cache and path.exists(sc_html_file_to_author_file):
      with open(sc_html_file_to_author_file) as f:
        self.SC_HTML_FILE_TO_AUTHOR = json.loads(f.read())
    else:
      self.SC_HTML_FILE_TO_AUTHOR = self.get_html_file_to_author_mapping()
      with open(sc_html_file_to_author_file, 'w') as f:
        f.write(json.dumps(self.SC_HTML_FILE_TO_AUTHOR))
    log('done')

    log('creating reference to html file mapping')
    reference_to_html_file = path.join(path.dirname(path.realpath(__file__)), '../data/cache/reference_to_html.json')
    if use_cache and path.exists(reference_to_html_file):
      with open(reference_to_html_file) as f:
        self.REFERENCE_TO_HTML_FILE = json.loads(f.read())
    else:
      self.REFERENCE_TO_HTML_FILE = self.get_html_references()
      with open(reference_to_html_file, 'w') as f:
        f.write(json.dumps(self.REFERENCE_TO_HTML_FILE))
    log('done')

  def get_text_name_mapping(self):
    text_id_to_name = {}

    for json_file in glob(path.join(self.BILARA_DIR, 'root/misc/site/name/sutta/**/*_root-misc-site.json'), recursive=True):
      # exceptions
      if json_file.endswith('/sutta/g2dhp-name_root-misc-site.json'):
        continue

      if json_file.endswith('/sutta/pf-name_root-misc-site.json'):
        continue

      if json_file.endswith('/sutta/lzh-art-name_root-misc-site.json'):
        continue

      if json_file.endswith('/sutta/kf-name_root-misc-site.json'):
        continue

      if json_file.endswith('/sutta/kf-name_root-misc-site.json'):
        continue

      if json_file.endswith('/sutta/kf-name_root-misc-site.json'):
        continue

      splitted_filename = path.basename(json_file).split('_')
      assert len(splitted_filename) == 2

      prefix = splitted_filename[0]

      with open(json_file) as f:
        json_content = json.loads(f.read())

      for i, (key, text_name) in enumerate(json_content.items()):
        #print(json_file, type(text_name))
        assert type(text_name) == str
        prefix_key = prefix + ':' + str(i + 1) + '.'
        assert key.startswith(prefix_key)

        text_id = key[len(prefix_key):]
        assert not text_id in text_id_to_name or text_id == 'g2dhp'

        text_id_to_name[text_id] = text_name

    return text_id_to_name

  def get_html_references(self):
    """ Find all references linked in the SC html files. """

    # use grep because it's super fast
    # search for the id tags which are the anchors
    # this will also add other ids to the mapping, but that does not bother
    args = ['grep', '-o', '-P', '-R', r'id=\'.+?\'', path.join(self.SC_DATA_DIR, 'html_text')]
    out = subprocess.run(args, capture_output=True, universal_newlines=True)

    assert out.returncode == 0

    # create the mapping: ref/id => html file path
    ref_to_html_file = defaultdict(set)

    for line in out.stdout.split('\n'):
      if not line:
        continue

      # parse the grep stdout format which is "<filpath>:<matching string>"
      html_file, match_str = line.split(':', 1)
      assert html_file in self.HTML_FILES

      reference = match_str.replace('id=', '').replace('\'', '').strip()
      ref_to_html_file[reference].add(html_file)

    return { key: list(value) for key, value in ref_to_html_file.items() }

  def find_translation_links_bilara(self, text_id: str, segment_id: str):
    """ Generate all links to a text / segment for the BILARA data. """

    links = []
    for json_file in self.BILARA_JSON_TRANSLATION_FILES:
      json_file_basename = path.basename(json_file)

      # filter based on filename for text id
      if not json_file_basename.startswith(text_id + '_translation-'):
        continue

      splitted = json_file_basename.replace('.json', '').split('_translation-')
      assert splitted[0] == text_id

      language, author_id = splitted[1].split('-', 1)

      # TODO: handle this author correctly
      if author_id in ['laera-quaresma', 'team']:
        continue

      assert len(language) == 2 or language in ['jpn']
      if not author_id in self.BILARA_AUTHORS:
        print(author_id)

      assert author_id in self.BILARA_AUTHORS

      url = 'https://suttacentral.net/%s/%s/%s#%s' % (text_id, language, author_id, segment_id)

      start_excerpt = ''
      with open(json_file) as f:
        text = json.loads(f.read())

        if segment_id in text:
          start_excerpt = text[segment_id][:300]

      links.append({
        'language': language,
        'authorId': author_id,
        'authorName': self.BILARA_AUTHORS[author_id]['name'],
        # 'textId': text_id,
        # 'segmentId': segment_id,
        'excerpt': start_excerpt,
        'url': url
      })

    return links

  def find_translation_links_sc(self, text_id: str, pts_refs, coocurrent_refs, path_dirs):
    seen_links = set()
    sc_links = []

    for ref in pts_refs + coocurrent_refs:
      # TODO: default links

      if ref in self.REFERENCE_TO_HTML_FILE:
        for html_file in self.REFERENCE_TO_HTML_FILE[ref]:
          # parse file path to create link
          html_file_base = path.join(self.SC_DATA_DIR, 'html_text')
          html_path_dirs = html_file.replace(html_file_base + '/', '').split('/')
          language = html_path_dirs[0]

          # check if the reference is actually corresponding to the current html file
          if path_dirs[:-1] != html_path_dirs[2:2 + len(path_dirs[:-1])]:
            continue

          assert html_file in self.SC_HTML_FILE_TO_AUTHOR
          author_id = self.SC_HTML_FILE_TO_AUTHOR[html_file]

          assert author_id in self.SC_AUTHOR_ID_TO_LONG

          if (text_id, author_id, language) in seen_links:
            # prevent multiple links for the same translation
            continue

          url = 'https://suttacentral.net/%s/%s/%s#%s' % (text_id, language, author_id, ref)
          sc_links.append({
            'language': language,
            'authorId': author_id,
            'authorName': self.SC_AUTHOR_ID_TO_LONG[author_id],
            'url': url
          })

          seen_links.add((text_id, author_id, language))

    return sc_links

  def get_html_file_to_author_mapping(self):
    args = ['grep', '-R', '-m1', "<meta name='author'", path.join(self.SC_DATA_DIR, 'html_text')]
    out = subprocess.run(args, capture_output=True, universal_newlines=True)
    assert out.returncode == 0

    result = {}
    for line in out.stdout.split('\n'):
      if not line:
        continue

      html_file, match_str = line.split(':', 1)
      assert html_file in self.HTML_FILES

      author_long_match = re.search(r"content='([^']+)", match_str)
      assert author_long_match != None

      author_long = author_long_match.group(1).replace('&amp;', '&')

      if author_long in self.SC_AUTHOR_LONG_TO_ID:
        result[html_file] = self.SC_AUTHOR_LONG_TO_ID[author_long]
      elif author_long in self.SC_AUTHOR_SHORT_TO_ID:
        result[html_file] = self.SC_AUTHOR_SHORT_TO_ID[author_long]
      else:
        assert False, 'no author for ' + str(author_long)

    return result

  def get_references(self):
    pts_data = []

    for ref_file in self.REFERENCE_FILES:
      path_dirs = ref_file.replace(self.REFERENCE_DIR, '').split('/')

      basket = path_dirs[0]
      #assert basket == 'sutta'

      book = self.normalize_book_abbreviation(
        path_dirs[2] if path_dirs[1] == 'kn' else path_dirs[1]
      )

      text_id = path.basename(ref_file.split('_')[0])

      with open(ref_file) as f:
        ref_data = json.loads(f.read())

      for segment_id, refs in ref_data.items():
        all_refs = [ref.strip() for ref in refs.split(',')]
        pts_refs = [ref for ref in all_refs if ref.startswith('pts-vp-pli')]
        non_pts_refs = [ref for ref in all_refs if not ref in pts_refs]

        root_text_json = path.join(
          self.BILARA_DIR,
          'root/pli/ms/',
          '/'.join(path_dirs[:-1]),
          text_id + '_root-pli-ms.json'
        )

        excerpt = ''
        with open(root_text_json) as f:
          try:
            excerpt = json.loads(f.read())[segment_id][:300]
          except Exception as e:
            print('No excerpt for', root_text_json, segment_id)



        default_link = {
          'language': 'pli',
          'authorId': 'ms',
          'authorName': 'Mahāsaṅgīti Tipiṭaka Buddhavasse 2500',
          'url': 'https://suttacentral.net/%s/pli/ms#%s' % (text_id, segment_id),
          'excerpt': excerpt
        }

        bilara_links = self.find_translation_links_bilara(text_id, segment_id)

        # TODO: many links dont seem to work, so don't include (https://github.com/suttacentral/suttacentral/projects/7#card-60833967)
        # sc_links = self.find_translation_links_sc(text_id, pts_refs, non_pts_refs, path_dirs)
        sc_links = []
        links = [default_link] + bilara_links + sc_links

        text_name = self.TEXT_ID_TO_NAME[text_id] if text_id in self.TEXT_ID_TO_NAME else ''


        for pts_ref in pts_refs:
          edition, volume, page = ExtractReferences.parse_pts_id(pts_ref)

          if book == 'kd':
            if volume == 1:
              book = BOOK_NAMES['Mahāvagga']
            elif volume == 2:
              book = BOOK_NAMES['Cūḷavagga']
            else:
              assert False

          pts_data.append({
            'datasource': 'suttacentral.net',
            # 'ptsId': pts_ref,
            'edition': edition,
            'book': book,
            'volume': volume,
            'page': page,
            # 'basket': basket,
            'textId': text_id,
            'textName': text_name,
            # 'segmentId': segment_id,
            # 'references': all_refs,
            'links': links
          })

    return pts_data

  @classmethod
  def parse_pts_id(cls, pts_id: str):
    m = re.search(r'pts-vp-pli(1ed|2ed|)([0-9]+)(\.[0-9]+)?', pts_id)

    edition = int(m.group(1)[0]) if m.group(1) else 0
    volume = 0
    page = 0

    if m.group(3):
      volume = int(m.group(2))
      page = int(m.group(3)[1:]) if m.group(3) else ''
    else:
      page = int(m.group(2))

    return edition, volume, page

  def normalize_book_abbreviation(self, book: str):
    if book in ['pli-tv-bi-vb', 'pli-tv-bu-vb', 'pli-tv-kd', 'pli-tv-pvr']:
      return BOOK_NAMES['Vinaya']

    return self.BOOK_ABBREVS[self.UID_EXPANSION[book]['name'].replace(' Nikāya', 'nikāya').replace('Nettipakaraṇapāḷi', 'Nettippakaraṇapāḷi').replace('Sutta Nipāta', 'Suttanipāta').replace('Saṁyuttanikāya', 'Saṃyuttanikāya').replace('Buddhavaṁsa', 'Buddhavaṃsa')]
