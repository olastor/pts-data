import json
from os import path
from parser.extract_references import ExtractReferences

if __name__ == '__main__':
  BILARA_DATA_DIR = path.join(path.dirname(path.realpath(__file__)), 'data/data/bilara-data-published')
  SC_DATA_DIR = path.join(path.dirname(path.realpath(__file__)), 'data/data/sc-data-master')
  DIST_DIR = path.join(path.dirname(__file__), '../../../dist/')

  use_cache = True
  extract_pts = ExtractReferences(BILARA_DATA_DIR, SC_DATA_DIR, use_cache)
  references = extract_pts.get_references()

  with open(path.join(DIST_DIR, 'scbil.json'), 'w') as f:
    f.write(json.dumps(references, indent=2))