import unittest
import json
from os import path
from parser.extract_references import ExtractReferences

BILARA_DATA_DIR = path.join(path.dirname(path.realpath(__file__)), '../../../bilara-data')
SC_DATA_DIR = path.join(path.dirname(path.realpath(__file__)), '../../../sc-data')
DIST_DIR = path.join(path.dirname(path.realpath(__file__)), '../../../../dist')

def normalize_name(s: str):
  return s.lower().replace(' ', '').replace('ī', 'i').replace('ā', 'a').replace('ṁ', 'm').replace('ṃ', 'm').replace('ñ', 'n').replace('samgha', 'saṅgha').replace('vaggo', 'vagga').replace('panho', 'panha').replace('suttam', 'sutta').replace('bhaṅgo', 'bhaṅga').replace('ddeso', 'ddesa')

class TestDataCompatibility(unittest.TestCase):

    def test_upper(self):
      #extract_pts = ExtractReferences(BILARA_DATA_DIR, SC_DATA_DIR)
      #references = extract_pts.get_references()
      with open(path.join(DIST_DIR, 'scbil.json')) as f:
        references = json.loads(f.read())

      with open(path.join(DIST_DIR, 'tporg_text.json')) as f:
        tporg_data = json.loads(f.read())

      # get intersection of references
      tp_refs = {}
      for item in tporg_data:
        for ref in item['pts_refs']:
          tp_refs[(item['book'], ref['volume'], ref['page'])] = normalize_name(
            ref['chapter'] + ' ' + ref['subhead']
          )

      sc_refs = {}
      for ref in references:
        if ref['edition'] != 0:
          continue

        sc_refs[(ref['book'], ref['volume'], ref['page'])] = normalize_name(ref['textName'])

      known_exceptions = set([
        ('mahasatipaṭṭhanasutta',
          '1.mūlapariyayavaggakayanupassananavasivathikapabbam'),
        ('bhayasutta', '(7)2.mahavagga1.titthayatanadisutta'),
        ('taṇhasutta', '(7)2.yamakavagga2.ḍtaṇhasutta'),
        ('mahasatipaṭṭhanasutta',
          '1.mūlapariyayavaggadhammanupassanabojjhaṅgapabbam'),
        ('vinayapeyyalavagga', '3.vinayapeyyalam'),
        ('mahasatipaṭṭhanasutta', '1.mūlapariyayavaggadhammanupassanakhandhapabbam'),
        ('seriṇipetivatthu', '3.cūḷavagga6.seriṇipetavatthu'),
        ('kodhapeyyalavagga', '1.kodhapeyyalam'),
        ('akusalapeyyalavagga', '2.akusalapeyyalam'),
        ('sabbasavasutta', '1.mūlapariyayavaggabhavanapahatabbasava'),
        ('sabbasavasutta', '1.mūlapariyayavaggadassanapahatabbasava'),
        ('mahasatipaṭṭhanasutta', '1.mūlapariyayavaggauddeso'),
        ('mahasatipaṭṭhanasutta', '1.mūlapariyayavaggacittanupassana'),
        ('sabbasavasutta', '1.mūlapariyayavaggaparivajjanapahatabbasava'),
        ('sabbasavasutta', '1.mūlapariyayavaggasamvarapahatabbasava'),
        ('mahasatipaṭṭhanasutta',
          '1.mūlapariyayavaggakayanupassanadhatumanasikarapabbam'),
        ('cittapariyadanavagga', '1.rūpadivagga'),
        ('mahasatipaṭṭhanasutta', '1.mūlapariyayavaggakayanupassanairiyapathapabbam'),
        ('bhikkhusamghapariharaṇapanha',
          '4.meṇḍakapanha7.bhikkhusaṅghapariharaṇapanha'),
        ('mahasatipaṭṭhanasutta', '1.mūlapariyayavaggamaggasaccaniddeso'),
        ('potaliyasutta', '1.gahapativaggakamadinavakatha'),
        ('paṭhamaaggasutta', '(8)3.apaṇṇakavagga5.dutiyaaggasutta'),
        ('samkhittūposathasutta', '5.uposathavagga1.saṅkhittūposathasutta'),
        ('titthayatanasutta', '(7)2.mahavagga1.titthayatanadisutta')
      ])

      # TODO: manually go through these and verify as known exceptions
      unchecked_exceptions = set([
        ('abhisamayakatha', '3.pannavaggadasaiddhiniddesa'),
        ('adhiṭṭhanaharasampata', '4.paṭiniddesavaro14.adhiṭṭhanaharasampato'),
        ('ajjhattaaniccahetusutta', '1.saḷayatanasamyuttam7.ajjhattaniccahetusutta'),
        ('anapanassatikatha', '1.mahavagga1.gaṇanavara'),
        ('anapanassatikatha', '1.mahavagga2.soḷasanaṇaniddesa'),
        ('anapanassatikatha', '1.mahavagga4.vodananaṇaniddesa'),
        ('anapanassatikatha', '1.mahavagga5.satokarinaṇaniddesa'),
        ('anapanassatikatha', '1.mahavagga6.naṇarasichakkaniddesa'),
        ('anapanassatikatha', '1.mahavaggacatutthacatukkaniddesa'),
        ('anapanassatikatha', '1.mahavaggadutiyacatukkaniddesa'),
        ('anapanassatikatha', '1.mahavaggadutiyacchakkam'),
        ('anapanassatikatha', '1.mahavaggapaṭhamacatukkaniddesa'),
        ('anapanassatikatha', '1.mahavaggatatiyacatukkaniddesa'),
        ('anapanassatikatha', '1.mahavaggatatiyacchakkam'),
        ('appamadavagga', '3.satipaṭṭhanasamyuttam1-10.tathagatadisuttadasakam'),
        ('arambhakatha', 'milindapanhapaḷi'),
        ('bahirakatha', 'milindapanhapaḷipubbayogadi'),
        ('balakaraṇiyavagga', '2.bojjhaṅgasamyuttam1-12.baladisutta'),
        ('balakaraṇiyavagga',
          '5.sammappadhanasamyuttam1-12.balakaraṇiyadisuttadvadasakam'),
        ('balakaraṇiyavagga', '9.jhanasamyuttam1-12.jhanadisuttadvadasakam'),
        ('balakatha', '2.yuganaddhavagga3.iddhipadavaro'),
        ('bojjhaṅgakatha', '2.yuganaddhavagga3.dutiyasuttantaniddesa'),
        ('bojjhaṅgakatha', '2.yuganaddhavaggamūlamūlakadidasakam'),
        ('bojjhaṅgakatha', '2.yuganaddhavaggasuttantaniddesa'),
        ('cariyakatha', '3.pannavagga2.indriyaniddesa'),
        ('catubyūhaharasampata', '4.paṭiniddesavaro6.catubyūhaharasampato'),
        ('cūḷabyūhasuttaniddesa', '12.cūḷaviyūhasuttaniddesa'),
        ('desanaharasampata', '4.paṭiniddesavaro1.desanaharasampato'),
        ('dhamma(nava)sutta', '2.cūḷavagga8.navasutta'),
        ('dhammacakkakatha', '2.yuganaddhavagga1.saccavaro'),
        ('dhammacakkakatha', '2.yuganaddhavagga2.satipaṭṭhanavaro'),
        ('dhammacakkakatha', '2.yuganaddhavagga3.iddhipadavaro'),
        ('diṭṭhikatha', '1.mahavagga1.assadadiṭṭhiniddesa'),
        ('diṭṭhikatha', '1.mahavagga10-12.sannojanikadidiṭṭhiniddesa'),
        ('diṭṭhikatha', '1.mahavagga13.attavadapaṭisamyuttadiṭṭhiniddesa'),
        ('diṭṭhikatha', '1.mahavagga15-16.bhava-vibhavadiṭṭhiniddesa'),
        ('diṭṭhikatha', '1.mahavagga2.attanudiṭṭhiniddesa'),
        ('diṭṭhikatha', '1.mahavagga4.sakkayadiṭṭhiniddesa'),
        ('diṭṭhikatha', '1.mahavagga6.ucchedadiṭṭhiniddesa'),
        ('diṭṭhikatha', '1.mahavagga7.antaggahikadiṭṭhiniddesa'),
        ('diṭṭhikatha', '1.mahavagga72-73.sabbannutanaṇaniddesa'),
        ('dutiyakalyaṇamittasutta', '1.maggasamyuttam1.kalyaṇamittasutta'),
        ('dutiyasilasampadadisuttapancaka',
          '1.maggasamyuttam2-6.silasampadadisuttapancakam'),
        ('dutiyayonisomanasikarasampadasutta',
          '1.maggasamyuttam7.yonisomanasikarasampadasutta'),
        ('esanavagga', '5.sammappadhanasamyuttam1-10.esanadisuttadasakam'),
        ('esanavagga', '7.iddhipadasamyuttam1-12.gaṅganadiadisuttadvadasakam'),
        ('gatikatha', '1.mahavagga2.niddesa'),
        ('gaṅgapeyyalavagga', '2.bojjhaṅgasamyuttam1-12.gaṅganadiadisutta'),
        ('gaṅgapeyyalavagga', '4.indriyasamyuttam1-12.pacinadisuttadvadasakam'),
        ('gaṅgapeyyalavagga', '5.sammappadhanasamyuttam1-12.pacinadisuttadvadasakam'),
        ('gaṅgapeyyalavagga', '6.balasamyuttam1-12.baladisuttadvadasakam'),
        ('gaṅgapeyyalavagga', '7.iddhipadasamyuttam1-12.gaṅganadiadisuttadvadasakam'),
        ('gaṅgapeyyalavagga', '9.jhanasamyuttam1-12.jhanadisuttadvadasakam'),
        ('haravibhaṅgapancamabhūmi', '5.pancamabhūmi'),
        ('iddhikatha', '3.pannavagga2.puggalavisesaniddesa'),
        ('iddhikatha', '3.pannavaggadasaiddhiniddesa'),
        ('indriyakatha', '1.mahavagga1.paṭhamasuttantaniddesa'),
        ('indriyakatha', '1.mahavagga2.dutiyasuttantaniddesa'),
        ('indriyakatha', '1.mahavagga3.tatiyasuttantaniddesa'),
        ('indriyakatha', '1.mahavagga5.indriyasamodhanam'),
        ('indriyakatha', '1.mahavaggaca.patiṭṭhapakaṭṭhaniddesa'),
        ('indriyakatha', '1.mahavaggaga.adhimattaṭṭhaniddesa'),
        ('indriyakatha', '1.mahavaggaga.caraviharaniddesa'),
        ('indriyakatha', '1.mahavaggaga.nissaraṇaniddesa'),
        ('indriyakatha', '1.mahavaggaka.adhipateyyaṭṭhaniddesa'),
        ('indriyakatha', '1.mahavaggaka.assadaniddesa'),
        ('indriyakatha', '1.mahavaggaka.pabhedagaṇananiddesa'),
        ('indriyakatha', '1.mahavaggakha.adinavaniddesa'),
        ('indriyakatha', '1.mahavaggakha.cariyavaro'),
        ('jalabujadanūpakarasutta',
          '9.supaṇṇasamyuttam17-46.jalabujadidanūpakarasuttatimsakam'),
        ('jatidhammasutta', '1.saḷayatanasamyuttam1-10.jatidhammadisuttadasakam'),
        ('kammakatha', '1.mahavagga2.niddesa'),
        ('kapilasutta(dhammacariyasutta)', '2.cūḷavagga6.dhammacariyasutta'),
        ('lokuttarakatha', '2.yuganaddhavagga3.iddhipadavaro'),
        ('maggakatha', '1.mahavagga2.niddesa'),
        ('mahabyūhasuttaniddesa', '13.mahaviyūhasuttaniddesa'),
        ('mahapannakatha', '3.pannavagga'),
        ('mahapannakatha', '3.pannavagga1.soḷasapannaniddesa'),
        ('mahapannakatha', '3.pannavagga2.puggalavisesaniddesa'),
        ('mahasatipaṭṭhanasutta', '1.mūlapariyayavaggamaggasaccaniddesa'),
        ('mahasatipaṭṭhanasutta', '1.mūlapariyayavaggauddesa'),
        ('matika', '6.opammakathapanha'),
        ('matikakatha', '3.pannavagga2.indriyaniddesa'),
        ('maṇḍapeyyakatha', '1.mahavagga2.niddesa'),
        ('mettakatha', '2.yuganaddhavagga1.indriyavaro'),
        ('mettakatha', '2.yuganaddhavagga2.balavaro'),
        ('mettakatha', '2.yuganaddhavagga3.bojjhaṅgavaro'),
        ('mettakatha', '2.yuganaddhavagga4.maggaṅgavaro'),
        ('mettakatha', '2.yuganaddhavaggasuttantaniddesa'),
        ('milindapanhapucchavisajjana',
          '2-3.milindapanha16.arūpadhammavavatthanadukkarapanha'),
        ('natumhakasutta', '1.khandhasamyuttam1.natumhakamsutta'),
        ('naṇakatha', '1.mahavagga1.sutamayanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga10.gotrabhunaṇaniddesa'),
        ('naṇakatha', '1.mahavagga11.magganaṇaniddesa'),
        ('naṇakatha', '1.mahavagga12.phalanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga13.vimuttinaṇaniddesa'),
        ('naṇakatha', '1.mahavagga14.paccavekkhaṇanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga15.vatthunanattanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga16.gocarananattanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga17.cariyananattanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga18.bhūminanattanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga19.dhammananattanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga2.silamayanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga25-28.paṭisambhidanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga29-31.naṇattayaniddesa'),
        ('naṇakatha', '1.mahavagga3.samadhibhavanamayanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga32.anantarikasamadhinaṇaniddesa'),
        ('naṇakatha', '1.mahavagga33.araṇaviharanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga34.nirodhasamapattinaṇaniddesa'),
        ('naṇakatha', '1.mahavagga35.parinibbananaṇaniddesa'),
        ('naṇakatha', '1.mahavagga36.samasisaṭṭhanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga37.sallekhaṭṭhanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga38.viriyarambhanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga39.atthasandassananaṇaniddesa'),
        ('naṇakatha', '1.mahavagga4.dhammaṭṭhitinaṇaniddesa'),
        ('naṇakatha', '1.mahavagga40.dassanavisuddhinaṇaniddesa'),
        ('naṇakatha', '1.mahavagga43.padesaviharanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga44-49.chavivaṭṭanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga5.sammasananaṇaniddesa'),
        ('naṇakatha', '1.mahavagga51.sotadhatuvisuddhinaṇaniddesa'),
        ('naṇakatha', '1.mahavagga52.cetopariyanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga53.pubbenivasanussatinaṇaniddesa'),
        ('naṇakatha', '1.mahavagga54.dibbacakkhunaṇaniddesa'),
        ('naṇakatha', '1.mahavagga55.asavakkhayanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga56-63.saccanaṇacatukkadvayaniddesa'),
        ('naṇakatha', '1.mahavagga6.udayabbayanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga64-67.suddhikapaṭisambhidanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga68.indriyaparopariyattanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga69.asayanusayanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga7.bhaṅganupassananaṇaniddesa'),
        ('naṇakatha', '1.mahavagga70.yamakapaṭihiranaṇaniddesa'),
        ('naṇakatha', '1.mahavagga71.mahakaruṇanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga72-73.sabbannutanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga8.adinavanaṇaniddesa'),
        ('naṇakatha', '1.mahavagga9.saṅkharupekkhanaṇaniddesa'),
        ('niddesavara', '3.niddesavarodvadasapada'),
        ('niddesavara', '3.niddesavaroharasaṅkhepo'),
        ('nigamana', '6.opammakathapanha7.issasaṅgapanha'),
        ('nigrodhakappa(vaṅgisa)sutta', '2.cūḷavagga12.nigrodhakappasutta'),
        ('oghavagga', '3.satipaṭṭhanasamyuttam1-10.uddhambhagiyadisuttadasakam'),
        ('oghavagga', '4.indriyasamyuttam1-10.oghadisuttadasakam'),
        ('oghavagga', '5.sammappadhanasamyuttam1-10.oghadisuttadasakam'),
        ('oghavagga', '6.balasamyuttam1-10.oghadisuttadasakam'),
        ('oghavagga', '9.jhanasamyuttam1-10.oghadisutta'),
        ('otaraṇaharasampata', '4.paṭiniddesavaro12.otaraṇaharasampato'),
        ('padaṭṭhanaharasampata', '4.paṭiniddesavaro4.padaṭṭhanaharasampato'),
        ('pakiṇṇakaniddesa', '7.harasampatabhūmi'),
        ('parivattanaharasampata', '4.paṭiniddesavaro9.parivattanaharasampato'),
        ('paṭhamajananakuhanasutta', '2.dukanipato8.paṭhamanakuhanasutta'),
        ('paṭhamanatumhakasutta', '1.saḷayatanasamyuttam5.paṭhamanatumhakamsutta'),
        ('paṭhamanatumhakasutta', '1.saḷayatanasamyuttam8.paṭhamanatumhakamsutta'),
        ('paṭihariyakatha', '3.pannavagga2.indriyaniddesa'),
        ('paṭisambhidakatha', '2.yuganaddhavagga1.dhammacakkapavattanavaro'),
        ('paṭisambhidakatha', '2.yuganaddhavagga2.satipaṭṭhanavaro'),
        ('paṭisambhidakatha', '2.yuganaddhavagga3.iddhipadavaro'),
        ('paṭisambhidakatha', '2.yuganaddhavagga4.sattabodhisattavaro'),
        ('paṭisambhidakatha', '2.yuganaddhavagga5.abhinnadivaro'),
        ('paṭisambhidakatha', '2.yuganaddhavagga8.paṭisambhidavaro'),
        ('punagaṅgapeyyalavagga', '2.bojjhaṅgasamyuttampunagaṅganadiadisutta'),
        ('punagaṅgapeyyalavagga', '4.indriyasamyuttam1-12.pacinadisuttadvadasakam'),
        ('punagaṅgapeyyalavagga', '6.balasamyuttam1-12.pacinadisuttadvadasakam'),
        ('punaoghavagga', '4.indriyasamyuttam1-10.oghadisuttadasakam'),
        ('punaoghavagga', '6.balasamyuttam1-10.oghadisuttadasakam'),
        ('puttamamsasutta', '1.nidanasamyuttam3.puttamamsūpamasutta'),
        ('pūraḷasa(sundarikabharadvaja)sutta',
          '3.mahavagga4.sundarikabharadvajasutta'),
        ('saccakatha', '2.yuganaddhavagga1.paṭhamasuttantaniddesa'),
        ('saccakatha', '2.yuganaddhavagga2.dhammuddhaccavaraniddesa'),
        ('saccakatha', '2.yuganaddhavagga2.dutiyasuttantapaḷi'),
        ('saccakatha', '2.yuganaddhavagga3.dutiyasuttantaniddesa'),
        ('samaropanaharasampata', '4.paṭiniddesavaro16.samaropanaharasampato'),
        ('samasisakatha', '3.pannavagga2.indriyaniddesa'),
        ('satipaṭṭhanakatha', '3.pannavagga2.indriyaniddesa'),
        ('saṅgahavara', '1.saṅgahavaro'),
        ('saṅkiccattheragatha', '11.ekadasanipato1.samkiccattheragatha'),
        ('subhūtittheragatha', ''),
        ('sucimukhisutta', '7.sariputtasamyuttam10.sūcimukhisutta'),
        ('sunnakatha', '2.yuganaddhavagga1.matika'),
        ('sunnakatha', '2.yuganaddhavagga2.niddesa'),
        ('sunnakatha', '2.yuganaddhavagga3.iddhipadavaro'),
        ('susimaparibbajakasutta', '1.nidanasamyuttam10.susimasutta'),
        ('tuvaṭakasuttaniddesa', '14.tuvaṭṭakasuttaniddesa'),
        ('uddesavara', '2.uddesavarotassanugiti'),
        ('uddesavara', '2.uddesavarotatridamuddanam'),
        ('vicayaharasampata', '4.paṭiniddesavaro2.vicayaharasampato'),
        ('vimokkhakatha', '1.mahavagga1.uddesa'),
        ('vimokkhakatha', '1.mahavagga2.niddesa'),
        ('vipallasakatha', '1.mahavagga2.niddesa'),
        ('vipassanakatha', '3.pannavagga2.indriyaniddesa'),
        ('viragakatha', '2.yuganaddhavagga4.maggaṅgavaro'),
        ('vivekakatha', '3.pannavagga1.maggaṅganiddesa'),
        ('vivekakatha', '3.pannavagga2.indriyaniddesa'),
        ('vivekakatha', '3.pannavaggadasaiddhiniddesa'),
        ('yuganaddhakatha', '2.yuganaddhavagga'),
        ('yuganaddhakatha', '2.yuganaddhavagga1.suttantaniddesa'),
        ('yuganaddhakatha', '2.yuganaddhavagga2.dhammuddhaccavaraniddesa')
      ])

      overlapping_refs = set(sc_refs.keys()) & set(tp_refs.keys())
      non_overlapping_refs = set(sc_refs.keys()) - overlapping_refs

      #overlapping_refs_matched = [(sc_refs[r], tp_refs[r]) for r in overlapping_refs if sc_refs[r] in tp_refs[r]]
      overlapping_refs_unmatched = [(r, sc_refs[r], tp_refs[r]) for r in overlapping_refs if not sc_refs[r] in tp_refs[r]]

      # Assert that all identical pts references in both datasources should resolve to the
      # same text (name). All unmatched entries are checked against our list of valid exceptions.
      self.assertEqual(
        set([x[1:] for x in overlapping_refs_unmatched]) - (known_exceptions | unchecked_exceptions),
        set()
      )

      books_tp = set([k[0] for k in tp_refs.keys()])
      books_sc = set([k[0] for k in sc_refs.keys()])
      overlapping_books = books_sc & books_tp

      # Assert that all references from SC that are not found in TP are from a book
      # that does not occur in TP.
      # TODO: check these
      unchecked_exceptions_non_overlapping = set([
        ('D', 2, 263),
        ('D', 3, 230),
        ('Pe', 0, 32),
        ('Pe', 0, 192),
        ('S', 4, 107),
        ('S', 5, 139),
        ('S', 5, 140)
      ])

      self.assertEqual(
        set([x for x in non_overlapping_refs if x[0] in overlapping_books]) - unchecked_exceptions_non_overlapping,
        set()
      )

if __name__ == '__main__':
  unittest.main()