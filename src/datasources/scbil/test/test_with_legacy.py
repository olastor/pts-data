import unittest
import json
import re
from os import path
from parser.extract_references import ExtractReferences, ROMAN_TO_INT, INT_TO_ROMAN

LEGACY_DATA_DIR = path.join(path.dirname(path.realpath(__file__)), '../data/legacy')
DIST_DIR = path.join(path.dirname(path.realpath(__file__)), '../../../../dist')
BILARA_DATA_DIR = path.join(path.dirname(path.realpath(__file__)), '../../../bilara-data')
SC_DATA_DIR = path.join(path.dirname(path.realpath(__file__)), '../../../sc-data')

LEGACY_BOOK_MAPPING = {
  'Vib': 'Vibh',
  'Nidd I': 'Nidd-I',
  'pe': 'Pe',
  'tha': 'Tha-ap',
  'thi': 'Thi-ap',
  'ya': 'Yam',
  'Nidd II': 'Nidd-II',
}

class TestDataCompatibility(unittest.TestCase):

    def test_compare_legacy(self):
      # extract_pts = ExtractReferences(BILARA_DATA_DIR, SC_DATA_DIR, True)
      # references = extract_pts.get_references()
      with open(path.join(DIST_DIR, 'scbil.json')) as f:
        references = json.loads(f.read())

      with open(path.join(LEGACY_DATA_DIR, 'pts_lookup.json')) as f:
        legacy_data = json.loads(f.read())

      book_legacy_to_new = {}
      for x in legacy_data.keys():
        edition = int(x[-4]) if x.endswith('ed)') else 0
        book = re.sub(r'\s+\([12]ed\)', '', x)

        if book in LEGACY_BOOK_MAPPING:
          book = LEGACY_BOOK_MAPPING[book]

        book_legacy_to_new[x] = (edition, book.lower())

      # TODO: books should map correctly
      self.assertEqual(
        set(),#book_intersection,
        set()
      )

      ref_lookup = [{}, {}, {}]
      ref_books = set()
      for ref in references:
        book = ref['book'].lower()

        ref_books.add((ref['edition'], book))
        if not book in ref_lookup[ref['edition']]:
          ref_lookup[ref['edition']][book] = {}

        if not ref['volume'] in ref_lookup[ref['edition']][book]:
          ref_lookup[ref['edition']][book][ref['volume']] = {}

        ref_lookup[ref['edition']][book][ref['volume']][ref['page']] = ref['textId']

      for key, (edition, book) in book_legacy_to_new.items():
        book = book.lower()
        volumes = [0]

        if not book in ref_lookup[edition]:
          print('unknown book', book)
          continue

        if type(legacy_data[key]) == dict:
          volumes = [ROMAN_TO_INT[x.upper()] for x in legacy_data[key].keys()]

        for vol in volumes:
          page_list = legacy_data[key] if vol == 0 else legacy_data[key][INT_TO_ROMAN[vol].lower()]

          for page, item in enumerate(page_list):
            if page == 0:
              continue

            if item == None:
              continue

            known_exceptions_pos = [
              (0, 'th', 0, 15),
              (0, 'th', 0, 22),
            ]

            if (edition, book, vol, page) in known_exceptions_pos:
              continue

            text_id = ref_lookup[edition][book][vol][page]

            known_exceptions_text_id = [
              'mil6.3.12',
              'an7.645-1124',
              'an8.148-627',
              'an9.113-432',
              'an10.229-232',
              'sn12.93-213',
              'sn12.93-213'
            ]

            if text_id in known_exceptions_text_id:
              continue

            if type(item) == list:
              if not text_id in item:
                print(text_id, item)
              self.assertTrue(text_id in item)
            else:
              self.assertEqual(
                text_id,
                item
              )

if __name__ == '__main__':
  unittest.main()