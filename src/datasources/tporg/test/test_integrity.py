import unittest
from os import path
import json
from parser.extract_references import parse_ref_number
from parser.parse_toc import parse_book

DIST_DIR = path.join(path.dirname(path.realpath(__file__)), '../../../../dist')

class TestDataIntegrity(unittest.TestCase):
  def test_uniquness(self):
    with open(path.join(DIST_DIR, 'tporg.json')) as f:
      data = json.loads(f.read())

    data_no_books = [x for x in data if not x['book']]
    uniq_refs = set([(x['edition'], x['book'], x['volume'], x['page']) for x in data_no_books])
    self.assertEqual(len(data_no_books) , len(uniq_refs))

if __name__ == '__main__':
  unittest.main()