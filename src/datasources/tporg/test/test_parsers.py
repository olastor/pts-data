import unittest
from os import path
from parser.extract_references import parse_ref_number
from parser.parse_toc import parse_book

class TestDataParsers(unittest.TestCase):
  def test_parse_book(self):
    self.assertEqual(parse_book('sdfjasdklf'), None)
    self.assertEqual(parse_book('Majjhimanikāya'), ('Majjhimanikāya', 'M'))
    self.assertEqual(parse_book('Majjhimanikāya-10'), ('Majjhimanikāya 10', 'M'))
    self.assertEqual(parse_book('Majjhimanikāya-aṭṭhakathā'), ('Majjhimanikāya-aṭṭhakathā', 'M-A'))
    self.assertEqual(parse_book('Majjhimanikāya-aṭṭhakathā-1'), ('Majjhimanikāya-aṭṭhakathā 1', 'M-A'))

  def test_parse_ref_num(self):
    self.assertEqual(parse_ref_number('4.12030'), (4, 12030))
    self.assertEqual(parse_ref_number('4.0030'), (4, 30))
    self.assertEqual(parse_ref_number('0.001'), (0, 1))
    self.assertRaises(AssertionError, lambda: parse_ref_number('asdad'))
    self.assertRaises(AssertionError, lambda: parse_ref_number('0.000'))

if __name__ == '__main__':
  unittest.main()