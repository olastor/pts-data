import unittest
from os import path
from parser.extract_references import parse_ref_number
from parser.parse_toc import parse_book

class TestDataCompleteness(unittest.TestCase):
  def test_parse_book(self):
    self.assertEqual(parse_book('sdfjasdklf'), None)
    self.assertEqual(parse_book('Majjhimanikāya'), ('Majjhimanikāya', 'M'))
    self.assertEqual(parse_book('Majjhimanikāya-10'), ('Majjhimanikāya 10', 'M'))
    self.assertEqual(parse_book('Majjhimanikāya-aṭṭhakathā'), ('Majjhimanikāya-aṭṭhakathā', 'M-A'))
    self.assertEqual(parse_book('Majjhimanikāya-aṭṭhakathā-1'), ('Majjhimanikāya-aṭṭhakathā 1', 'M-A'))

  def test_parse_ref_num(self):
    self.assertEqual(parse_ref_number('1.0001'), (1, 1))
    self.assertEqual(parse_ref_number('3.0101'), (3, 101))
    self.assertEqual(parse_ref_number('0.123'), (0, 123))
    self.assertRaises(AssertionError, lambda: parse_ref_number('12.123'))
    self.assertRaises(AssertionError, lambda: parse_ref_number('0123'))
    self.assertRaises(AssertionError, lambda: parse_ref_number('1.000'))
    self.assertRaises(AssertionError, lambda: parse_ref_number('sdfdsf'))

if __name__ == '__main__':
  unittest.main()