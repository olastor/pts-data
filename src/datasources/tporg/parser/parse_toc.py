import json
import re
from os import path
import xml.etree.ElementTree as ET
from functools import reduce

TIPITIKA_DIR = path.join(path.dirname(path.realpath(__file__)), '../data/xmlfiles/')

with open(path.join(path.dirname(path.realpath(__file__)), '../../../metadata/books.json')) as f:
  BOOK_NAMES = json.loads(f.read())

def get_or_create_item(d: dict, key: str):
  """ helper function for reduce. """
  if not key in d:
    d[key] = {}
  return d[key]

def parse_toc_file(toc_file: str):
  """ parse a toc file. """

  texts = []

  started = False
  ended = False
  level = 0
  for event, elem in ET.iterparse(path.join(TIPITIKA_DIR, toc_file), events=('start', 'end')):
    if elem.attrib == {}:
      ended = started
      started = True
      continue

    assert started
    assert not ended

    level += (1 if event == 'start' else -1) * 1
    assert level < 2 # assert flat structure

    assert elem.attrib['target'] == 'text'
    assert 'text' in elem.attrib
    assert 'action' in elem.attrib

    if event == 'start':
      texts.append((elem.attrib['text'], elem.attrib['action']))

  return texts

def parse_toc():
  """ parse the global toc files. """

  toc_tree = {}
  text_files = []

  tocs = []
  for event, elem in ET.iterparse(path.join(TIPITIKA_DIR, 'tipitaka_toc.xml')):
    text = elem.attrib['text'] if 'text' in elem.attrib else ''
    src = elem.attrib['src'] if 'src' in elem.attrib else ''

    if text and src:
      tocs.append((elem.attrib['text'], elem.attrib['src']))

  for toc_title, toc_file in tocs:
    current_path = [toc_title]
    for event, elem in ET.iterparse(path.join(TIPITIKA_DIR, toc_file), events=('start', 'end')):
      text = elem.attrib['text'] if 'text' in elem.attrib else ''
      src = elem.attrib['src'] if 'src' in elem.attrib else ''

      if src and event == 'start':
        # at the leaves, parse the toc files referenced
        parsed_toc = parse_toc_file(src)

        reduce(get_or_create_item, current_path, toc_tree)[text] = parsed_toc
        text_files += [
          (item, [text] + current_path[::-1])
          for item in parsed_toc
        ]

      if text:
        if event == 'start':
          current_path.append(text)
        elif event == 'end':
          assert current_path[-1] == elem.attrib['text']
          current_path = current_path[:-1]

  return toc_tree, text_files

def parse_book(maybe_a_book_name: str):
  """Parse a book name.

  Args:
      maybe_a_book_name (str): String to parse.

  Returns:
      (str, str): A tuple (book_name, book_abbrev). First on is the name of the book, second the abbreviation.
  """

  is_atthakatha = False
  suffix_number = None

  # because of "Dīgha nikāya (aṭṭhakathā)"
  maybe_a_book_name = maybe_a_book_name.replace('Dīgha nikāya', 'Dīghanikāya')

  # because of "Dhammasaṅgaṇi-aṭṭhakathā"
  maybe_a_book_name = maybe_a_book_name.replace('Dhammasaṅgaṇi', 'Dhammasaṅgaṇī')

  maybe_a_book_name = maybe_a_book_name.replace('Nettippakaraṇa', 'Nettipakaraṇa')

  match_suffix_number = re.search(r'(.*)(-[0-9]+)$', maybe_a_book_name)
  if match_suffix_number:
    maybe_a_book_name = match_suffix_number.group(1).strip()
    suffix_number = int(match_suffix_number.group(2).strip().replace('-', ''))
    assert suffix_number > 0

  # trim this suffix
  match_suffix_pali = re.match(r'^(.*)?(pāḷi)$', maybe_a_book_name)

  if match_suffix_pali:
    maybe_a_book_name = match_suffix_pali.group(1).strip()

  match_suffix_atthakatha = re.search(r'(.*)(\s*\(aṭṭhakathā\)|-aṭṭhakathā)$', maybe_a_book_name)
  if match_suffix_atthakatha:
    is_atthakatha = True
    maybe_a_book_name = match_suffix_atthakatha.group(1).strip()

  if maybe_a_book_name in BOOK_NAMES:
    book_name = '%s%s%s' % (
      maybe_a_book_name,
      '-aṭṭhakathā' if is_atthakatha else '',
      ' ' + str(suffix_number) if suffix_number else ''
    )

    book_abbrev = '%s%s' % (BOOK_NAMES[maybe_a_book_name], '-A' if is_atthakatha else '')

    return book_name, book_abbrev

  return None

def find_book_from_hierarchy(hier):
  #assert hier[-1] in 'Tipiṭaka (Mūla)'
  if hier[-2] == 'Vinayapiṭaka' and hier[0] == 'Pācittiyapāḷi':
      return 'Vibhaṅga', BOOK_NAMES['Vibhaṅga']

  if hier[-2] == 'Vinayapiṭaka' and hier[0] == 'Pārājikapāḷi':
      return 'Vibhaṅga', BOOK_NAMES['Vibhaṅga']

  for item in hier[::-1]:
    parsed = parse_book(item)

    if parsed:
      return parsed

  return '', ''