from os import path
from glob import glob
from collections import defaultdict
import xml.etree.ElementTree as ET
from operator import getitem
from functools import reduce
import re

def parse_ref_number(number_str: str):
  """ Parse volume and page number from format like '1.0123'. """

  assert re.match(r'^[0-9]\.0*[1-9][0-9]*$', number_str)

  volume = int(number_str.split('.')[0])
  page = int(number_str.split('.')[1])

  return volume, page

def extract_references(xml_file: str):
  """ Parse xml file and extract pts reference with position. """

  refs = []

  current_chapter = ''
  current_subhead = ''
  current_paragraph = ''

  for event, elem in ET.iterparse(xml_file):
    rend = elem.attrib['rend'] if 'rend' in elem.attrib else ''
    ed = elem.attrib['ed'] if 'ed' in elem.attrib else ''
    n = elem.attrib['n'] if 'n' in elem.attrib else ''

    if rend == 'paranum':
      assert elem.tag == 'hi'
      assert elem.text != None
      current_paragraph = elem.text
    elif rend == 'chapter':
      assert elem.tag == 'p'
      current_chapter = elem.text
    elif rend == 'subhead':
      assert elem.tag == 'p'
      current_subhead = elem.text
    elif ed == 'P' and n:
      assert elem.tag == 'pb'

      volume, page = parse_ref_number(n)
      start_excerpt = elem.tail[:300] if elem.tail else ''

      if xml_file.endswith('cscd/s0508a1.att3.xml') and volume == 1 and page == 92:
        # typo in the root data...
        volume = 2

      refs.append({
        'volume': volume,
        'page': page,
        'paragraph': current_paragraph,
        'chapter': current_chapter,
        'subhead': current_subhead,
        'excerpt': start_excerpt
      })

  return refs
