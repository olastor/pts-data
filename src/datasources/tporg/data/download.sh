#!/bin/bash

wget https://tipitaka.org/romn/cscd/romn.zip
wget https://tipitaka.org/romn/tipitaka_toc.xml
wget https://tipitaka.org/romn/toc1.xml
wget https://tipitaka.org/romn/toc2.xml
wget https://tipitaka.org/romn/toc3.xml
wget https://tipitaka.org/romn/toc4.xml
