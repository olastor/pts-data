import json
from os import path
from parser.extract_references import extract_references

if __name__ == '__main__':
  DHPA_HTML_DIR = path.join(path.dirname(__file__), 'data/dhpa/html')
  DIST_DIR = path.join(path.dirname(__file__), '../../../dist/')

  references = extract_references(DHPA_HTML_DIR)

  with open(path.join(DIST_DIR, 'abts-dhpa.json'), 'w') as f:
    f.write(json.dumps(references, indent=2))