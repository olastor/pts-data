import unittest
from os import path
from parser.extract_references import extract_references

DHPA_HTML_DIR = path.join(path.dirname(__file__), '../data/dhpa/html')

class TestDataCompleteness(unittest.TestCase):
  def test_completenes(self):
    references = extract_references(DHPA_HTML_DIR)

    known_exceptions = [
      (1, 161), #, 'https://www.ancient-buddhist-texts.net/English-Texts/Buddhist-Legends/02-01.htm#p_13'),
      (1, 289), #, 'https://www.ancient-buddhist-texts.net/English-Texts/Buddhist-Legends/03-01.htm#p_14'),
      (2, 14), #, 'https://www.ancient-buddhist-texts.net/English-Texts/Buddhist-Legends/05-01.htm#p_44'),
      (4, 5) #, 'https://www.ancient-buddhist-texts.net/English-Texts/Buddhist-Legends/23-01.htm#p_16')
    ]

    vol_prev = 0
    page_prev = 0
    link_prev = ''

    for i, ref in enumerate(references):
      vol = ref['volume']
      page = ref['page']
      link = ref['links'][0]['url']

      first_of_page = link.split('#')[0] != link_prev.split('#')[0]
      if not first_of_page and not (vol, page) in known_exceptions:
        if vol == vol_prev:
          self.assertEqual(page, page_prev + 1)
        else:
          self.assertEqual(vol, vol_prev + 1)
          self.assertEqual(page, 1)

      vol_prev = vol
      page_prev = page
      link_prev = link


if __name__ == '__main__':
  unittest.main()