#!/usr/bin/env python3

import json
from os import path
from pathlib import Path
from collections import defaultdict

DIST_DIR = path.join(path.dirname(__file__), '../dist/')

with open(path.join(DIST_DIR, 'tporg.json')) as f:
  tporg_data = json.loads(f.read())

with open(path.join(DIST_DIR, 'scbil.json')) as f:
  bilara = json.loads(f.read())

with open(path.join(DIST_DIR, 'abts-dhpa.json')) as f:
  abt_dhpa = json.loads(f.read())

with open(path.join(path.dirname(__file__), 'metadata/books.json')) as f:
  book_abbrev_to_name = { v: k for k, v in json.loads(f.read()).items() }

merged_data = defaultdict(list)

for ref in tporg_data + abt_dhpa + bilara:
  volume = ref['volume']
  book = ref['book']
  page = ref['page']

  if not book:
    continue

  del ref['book']
  del ref['volume']
  del ref['page']

  assert len(book) > 0
  assert volume >= 0
  assert page > 0

  if 'edition' not in ref:
    ref['edition'] = 0

  merged_data[(book, volume, page)].append(ref)

books = set()
structure = {}
for (book, vol, page), items in merged_data.items():
  if not book:
    continue

  book_name = book_abbrev_to_name[book[:-2]] + ' (Aṭṭhakathā)' if book.endswith('-A') else book_abbrev_to_name[book]
  books.add((book_name, book))

  if not book in structure:
    structure[book] = {}

  if not str(vol) in structure[book]:
    structure[book][str(vol)] = []

  structure[book][str(vol)].append(page)
  structure[book][str(vol)] = sorted(structure[book][str(vol)])

structure_compressed = structure.copy()
for book, values in structure_compressed.items():
  for volume, pages in values.items():
    pages_sorted = sorted(pages)
    start = min(pages_sorted)
    end = max(pages_sorted)
    exceptions = [i for i in range(start, end) if not i in pages_sorted]

    structure_compressed[book][volume] = {
      'start': start,
      'end': end,
      'exceptions': exceptions
    }

Path(path.join(DIST_DIR, 'api', 'lookup')).mkdir(parents=True, exist_ok=True)

for book, volume in set([p[:2] for p in merged_data.keys()]):
  folder = path.join(DIST_DIR, 'api/lookup', book.lower(), str(volume))
  Path(folder).mkdir(parents=True, exist_ok=True)

for (book, volume, page), data in merged_data.items():
  folder = path.join(DIST_DIR, 'api/lookup', book.lower(), str(volume))

  with open(path.join(folder, '%i.json' % (page)), 'w') as f:
    f.write(json.dumps(data))

with open(path.join(DIST_DIR, 'api', 'structure.json'), 'w') as f:
  f.write(json.dumps(structure_compressed))

with open(path.join(DIST_DIR, 'api', 'books.json'), 'w') as f:
  f.write(json.dumps([{ 'name': name, 'abbrev': abbrev } for name, abbrev in books]))